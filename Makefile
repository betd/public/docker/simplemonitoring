SHELL=/bin/sh

## Colors
ifndef VERBOSE
.SILENT:
endif

REGISTRY_DOMAIN=registry.gitlab.com
IMAGE_PATH=/betd/public/docker/simplemonitoring
VERSION=latest

COLOR_COMMENT=\033[0;32m
COLOR_INFO=\033[0;33m
COLOR_RESET=\033[0m


.PHONY: flake8 dist twine twine-test

flake8:
	pipenv run flake8 --ignore=E501,W503,E203 *.py simplemonitor/

integration-tests:
	pipenv run env PATH="$(PWD)/tests/mocks:$(PATH)" "$(ENVPATH)/bin/coverage" run monitor.py -1 -v -d -f tests/monitor.ini

env-test:
	pipenv run env TEST_VALUE=myenv "$(ENVPATH)/bin/coverage" run --append monitor.py -t -f tests/monitor-env.ini

unit-test:
	pipenv run "$(ENVPATH)/bin/coverage" run --append -m unittest discover -s tests

network-test:
	pipenv run tests/test-network.sh

dist:
	rm -f dist/simplemonitor-*
	pipenv run python setup.py sdist bdist_wheel

twine-test:
	pipenv run python -m twine upload --repository-url https://test.pypi.org/legacy/ dist/*

twine:
	pipenv run python -m twine upload dist/*

black:
	pipenv run black --check --diff *.py simplemonitor/

## Help
help:
	printf "${COLOR_COMMENT}Usage:${COLOR_RESET}\n"
	printf " make [target]\n\n"
	printf "${COLOR_COMMENT}Available targets:${COLOR_RESET}\n"
	awk '/^[a-zA-Z\-\_0-9\.@]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf " ${COLOR_INFO}%-16s${COLOR_RESET} %s\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)

## build and push image
all: build push_image

## build image and tags it
build:
	rm -f docker-compose-hacked.yml
	sed "/monitor.Dockerfile/ a \ \ \ \ image: ${REGISTRY_DOMAIN}${IMAGE_PATH}/monitor:${VERSION}" docker-compose.yml > docker-compose-hacked.yml
	sed -i "/webserver.Dockerfile/ a \ \ \ \ image: ${REGISTRY_DOMAIN}${IMAGE_PATH}/webserver:${VERSION}" docker-compose-hacked.yml
	docker-compose -f docker-compose-hacked.yml build monitor
	docker-compose -f docker-compose-hacked.yml build webserver
	rm -f docker-compose-hacked.yml

.PHONY: build

## login on registry
registry_login:
	docker login registry.gitlab.com

## push image
push_image: registry_login
	docker push ${REGISTRY_DOMAIN}${IMAGE_PATH}/monitor:${VERSION}
	docker push ${REGISTRY_DOMAIN}${IMAGE_PATH}/webserver:${VERSION}

.DEFAULT_GOAL := help
